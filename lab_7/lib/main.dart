import 'dart:ui';
import 'dart:async';

import 'package:flutter/material.dart';

String global_admin_type = 'generator';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of the application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Remove debug banner
      debugShowCheckedModeBanner: false,
      title: 'BansosPeduli',

      // Set application theme
      home: Theme(
              data: ThemeData(
              primarySwatch: Colors.blue,
              textTheme: const TextTheme(
                bodyText2: TextStyle(
                  fontSize: 19.0,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Roboto',
                ),
              ),
              scaffoldBackgroundColor: Colors.white,
            ),
            child: InputForm(),
          ),
    );
  }
}

class InputForm extends StatefulWidget {
  const InputForm({ Key? key }) : super(key: key);

  @override
  _InputFormState createState() => _InputFormState();
}

class _InputFormState extends State<InputForm> {

  final _formKey = GlobalKey<FormState>();
  TextEditingController _provinceCodeController = TextEditingController();
  TextEditingController _provinceNameController = TextEditingController();

  String provinceCode = '';
  String provinceName = '';
  List<String> registeredProvince = [];
  bool flagDuplicate = false;
  bool hideMessage = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: const [
            Icon(
              Icons.pin_drop,
            ),
            Text('BansosPeduli'),
          ],
        ),
      ),

      drawer: AdminBasedDrawer(global_admin_type),

      body: Form(
        key: _formKey,
        child:SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(20.0, 25.0, 20.0, 0),
                child: Column(
                  children: [
                    Container(
                      color: Colors.blue,
                      padding: EdgeInsets.all(5),
                      child: Text(
                        'Register Area',
                        style: TextStyle(
                          fontSize: 35,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    

                    const SizedBox(
                      height: 20,
                    ),

                    Padding(
                      padding: const EdgeInsets.fromLTRB(3.0, 8.0, 8.0, 3.0),
                      child: TextFormField(
                        controller: _provinceCodeController,
                        decoration: InputDecoration(
                          fillColor: Color.fromRGBO(238, 238, 238, 0.93),
                          filled: true,
                          hintText: "contoh: JB",
                          labelText: "Province Code",
                          prefixIcon: Icon(Icons.confirmation_number_rounded),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50.0)),
                        ),

                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Province Code should not be empty.';
                          }
                          return null;
                        },
                      ),
                    ),
                    
                    SizedBox(
                      height: 10,
                    ),

                    Padding(
                      padding: const EdgeInsets.fromLTRB(3.0, 8.0, 8.0, 3.0),
                      child: TextFormField(
                        controller: _provinceNameController,

                        decoration: InputDecoration(
                          fillColor: Color.fromRGBO(238, 238, 238, 0.93),
                          filled: true,
                          hintText: "contoh: Jawa Barat",
                          labelText: "Province Name",
                          prefixIcon: Icon(Icons.location_city_rounded),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(50.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Province name should not be empty.';
                          }
                          return null;
                        },
                      ),
                    ),

                    SizedBox(
                      height: 10,
                    ),

                    ElevatedButton(
                      child: const Text(
                        'Register',
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                      onPressed: (){
                        if(_formKey.currentState!.validate()) {
                          setState(() {
                            hideMessage = false;

                            provinceCode = _provinceCodeController.text;
                            provinceName = _provinceNameController.text;

                            if (registeredProvince.contains(provinceName)) {
                              flagDuplicate = true;

                            } else {
                              flagDuplicate = false;
                              registeredProvince.add(provinceName);
                            }
                          });

                        } else {
                          setState(() {
                            hideMessage = true;
                          });
                        }
                      },

                      style: ButtonStyle(
                        elevation: MaterialStateProperty.all<double>(0),
                      ),
                    ),

                    const SizedBox(
                      height: 10,
                    ),

                    if (!flagDuplicate && !hideMessage) Container(
                      padding: EdgeInsets.all(8),
                      color: Colors.blue[100],
                      child: Column(
                        children: [
                          const Text(
                            "Notes on Previous Entry:",
                            style: TextStyle(
                              fontSize: 13,
                            )
                          ),

                          Text(
                            "[Accepted] Province " + provinceName + " with code " + provinceCode + " was registed successfully.",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 13,
                            ),
                          ),
                        ]
                      ),

                    ) else if (!hideMessage) Container(
                      padding: EdgeInsets.all(8),
                      color: Colors.red[100],
                      child: Column(
                        children: [
                          const Text(
                            "Notes on Previous Entry:",
                            style: TextStyle(
                              fontSize: 13,
                            )
                          ),

                          Text(
                            "[Rejected] Province " + provinceName + " has been registered.",
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              fontSize: 13,
                            ),
                          ),
                        ]
                      ),
                    ),

                    const SizedBox(
                      height: 20,
                    ),

                    Image.network(
                      'https://www.drupal.org/files/project-images/reg_confirm_email_with_button_0.png',
                      height: 200,
                    ),

                  ],
                ),
              ),
            ],
          ),
        )
      ),
      
    );
  }
}




class AdminBasedDrawer extends StatelessWidget {

  final String adminType;

  AdminBasedDrawer(this.adminType);

  @override
  Widget build(BuildContext context) {
    String dashboardTitle;
    
    if(adminType == 'tracker') {
      dashboardTitle = 'Tracking Admin Dashboard';
    } else {
      dashboardTitle = 'Generator Admin Dashboard';
    }

    return Drawer(
        child: ListView(
          children: [
            ListTile(
              title: Align(
                child: Text(
                  'BansosPeduli',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontFamily: 'Roboto',
                    fontSize: 30,
                    fontWeight: FontWeight.w900,
                  ),
                ),
                alignment: Alignment(-1.0, 0),
              ),
            ),
            
            const Divider(
              color: Colors.grey,
            ),

            ListTile(
              leading: Icon(Icons.account_circle),
              title: const Align(
                child: Text('My Profile'),
                alignment: Alignment(-1.2, 0),
              ),

              onTap: (){
                print('My profile is clicked');
              },
            ),

            ListTile(
                leading: Icon(Icons.dashboard),
                title: Align(
                  child: Text(dashboardTitle),
                  alignment: Alignment(-1.9, 0),
                ),

                onTap: (){
                  if(adminType == 'tracker')
                    print('Tracking Dashboard is clicked');
                  else
                    print('Generator Dasboard is Clicked');
                },
            ),

            ListTile(
                leading: Icon(Icons.logout),
                title: const Align(
                  child: Text('Logout'),
                  alignment: Alignment(-1.2, 0),
                ),

                onTap: (){
                  print('Logout is clicked');
                },
            ),
          ]
        ),
      );
  }
}