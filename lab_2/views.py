from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers

def index(request):
    # Get all Note objects from database
    notes = Note.objects.all()
    context = {'notes':notes}
    # Render template lab2.html with argument context
    return render(request, 'lab2.html', context)

def xml(request):
    # Get all Note objects from database
    notes = Note.objects.all()
    # Take notes parameter instead of reinvoking Note.objects.all()
    # The serialize method converts complex data such as model
    # to be converted to native python datatypes, e.g. JSON and XML
    data = serializers.serialize('xml', notes)
    return HttpResponse(data, content_type="application/xml")

def json(request):
    # Get all Note objects from database
    notes = Note.objects.all()
    data = serializers.serialize('json', notes)
    return HttpResponse(data, content_type="application/json")