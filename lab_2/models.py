from django.db import models

# Note model consisting to, from, title, and message
class Note(models.Model):
    to = models.CharField(max_length=30, default='')
    fromNote = models.CharField(max_length=30, default='')
    title = models.CharField(max_length=50, null=True)
    # Message utilizes Text Field so that it can it
    # can store more than 255 characters
    message = models.TextField()