from django.contrib import admin
from lab_2.models import Note

# Register Note model to admin (to display it on admin site)
admin.site.register(Note)
