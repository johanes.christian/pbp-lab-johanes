from django.shortcuts import render, redirect
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
def index(request):
    # Get all Note objects from database
    notes = Note.objects.all()
    context = {'notes':notes}
    # Render template lab2.html with argument context
    return render(request, 'lab4_index.html', context)

def note_list(request):
    # Get all Note objects from database
    notes = Note.objects.all()
    context = {'notes':notes}
    # Render template lab2.html with argument context
    return render(request, 'lab4_note_list.html', context)


def add_note(request):
    # Instantiate form if the request method is GET
    if request.method == "GET":
        form = NoteForm()
    
    # Instantiate form, pass data from the request to the Form Object
    else:
        form = NoteForm(request.POST)

        # Check if form is valid ( if all data is correct
        # according to the domain and not empty--if not allowed)
        if form.is_valid():
            # Save the data of the form is it is valid to database
            form.save()

            # Redirect to note table after saving new note
            return redirect('/lab-4/')

    context = {'form':form}
    return render(request, 'lab4_form.html', context)