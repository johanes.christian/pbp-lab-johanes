from django import forms
from lab_2.models import Note


class NoteForm(forms.ModelForm):
    class Meta:
        # Let django know that the Note Model will 
        # be used as a basis in creating the form
        model = Note

        # Include the note to, fromNote, title, and message attributes 
        # in the form
        fields = ["to", "fromNote", "title", "message"]