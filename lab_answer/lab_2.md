# JSON and XML differences
According to geeksforgeeks, JSON is a data-interchange format that is language independent. On the other hand, XML or Extensible markup language is markup language designed to carry data. The difference of the two are as the following.
1. JSON is a javascript object notation where the XML is an extensible markup language. 
2. As JSON uses javascript object notation, it is used to represent objects, where XML is used to represent data items by using tag structure (similar to HTML)
3. JSON supports array where XML does not. 
4. JSON is a format written in JavaScript where XML is an independent markup language.
5. Information or data in JSON is stored as a mapping (or dictionary in python) where in XML the data are stored in a tree structure. 
6. JSON does not support comments, a feature supported by XML.
7. XML has a wider data type, it includes charts, images, primitive and non-primitive data type. On the other hand, JSON only support primitive data types that include boolean, numbers, objects, and strings. 

# HTML and XML differences
Similar to XML, HTML is a markup language but it is used to create web pages. Hence, in contrast to XML whose main purpose is to carry data, HTML is designed to display the data. The hypertext factor of the HTML is used in defining link between one page and the other where the markup laguage factor is used in structuring the web pages. The difference between HTML and XML is as the following.
1. The tag in HTML are predefined tags where users can define their own tags in XML.
2. As stated, XML is used to store and carry/transport data from the database where HTML is only used to display and render data at the client side. 
3. Syntaxwise, HTML can ignore small errors in its code. That is, it can still run although there is a typo in the tag or a tag that is not closed. On the other hand, XML does not ignore such errors. Correspondingly, XML is case sensitive where HTML is case insensitive. 
4. The nesting, i.e. defining that an element is under the scope of another element, must be done properly in XML. However, such nesting concept does not apply in HTML, given that it uses the open and closing tag as the basis.

## References
[GeeksForGeeks](https://www.geeksforgeeks.org/difference-between-json-and-xml/)\
[W3School](https://www.w3schools.com/js/js_json_xml.asp)\
[JavatPoint](https://www.javatpoint.com/html-vs-xml)\
[TechDifferences](https://techdifferences.com/difference-between-xml-and-html.html)