from django.urls import path
# Import index and friend_list from lab_1.views.py
from .views import index, friend_list


urlpatterns = [
    path('', index, name='index'),
    # Added friends path to url pattern
    # function friend_list will be accessed by 
    # <>/lab-1/friends
    path('friends', friend_list, name="friend_list")
]
