from django.contrib import admin
from .models import Friend


# Register Friend model to admin
admin.site.register(Friend)