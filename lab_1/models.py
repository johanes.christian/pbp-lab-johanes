from django.db import models
import datetime


# DONE Create Friend model that contains name, npm, and DOB (date of birth) here
class Friend(models.Model):
    # Name attribute and NPM of Friend is charfield
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=30, default='000000000')
    # DOB utilizes DataField models Field
    date_of_birth = models.DateField(default=datetime.date.today)
