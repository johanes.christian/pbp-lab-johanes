from django.urls import path, re_path, include
from .views import index, add_friend

# Maps URL Path to its corresponding function

app_name = 'lab_3'

urlpatterns = [
    path('', index, name="index"),
    path('add', add_friend, name="add_friend"),
]