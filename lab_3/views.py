from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from lab_1.models import Friend
from .forms import FriendForm


# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    # Get all Friend objects from database 
    friends = Friend.objects.all()
    response = {'friends': friends}
    # Render template friend_list_lab1.html with argument response
    return render(request, 'lab3_index.html', response)

# Method to add friend to the db
# Redirect user to login_url if user has not signed in
# or execute function if otherwise.
@login_required(login_url='/admin/login/')
def add_friend(request):
    if request.method == "POST":
        # Instantiate form, pass data from the request to the Form Object
        form = FriendForm(request.POST)
        # request.GET['']
        # Check if form is valid ( if all data is correct
        # according to the domain and not empty--if not allowed)
        if form.is_valid():
            # Save the data of the form is it is valid to database
            form.save()
            # Redirect to table page if form is validated and saved
            return redirect('lab_3:index')
    else:
        # Instantiate FriendForm and assign to form
        # when request type is GET
        form = FriendForm()

    context = {'form':form}

    return render(request, 'lab3_form.html', context)