from django import forms
from lab_1.models import Friend


class FriendForm(forms.ModelForm):
    class Meta:
        # Let django know that the Friend Model will 
        # be used as a basis in creating the form
        model = Friend
        # Specify which field of the Friend model
        # should be included in the Form
        fields = ['name','npm','date_of_birth']
    