import 'dart:ui';
import 'dart:async';

import 'package:flutter/material.dart';

String global_admin_type = 'tracker';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of the application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Remove debug banner
      debugShowCheckedModeBanner: false,
      title: 'BansosPeduli',

      // Set application theme
      home: Theme(
              data: ThemeData(
              primarySwatch: Colors.blue,
              textTheme: const TextTheme(
                bodyText2: TextStyle(
                  fontSize: 19.0,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'Roboto',
                ),
              ),
            ),
            child: Home(),
          ),
    );
  }
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: const [
            Icon(
              Icons.pin_drop,
            ),
            Text('BansosPeduli'),
          ],
        ),
      ),

      drawer: AdminBasedDrawer(global_admin_type),

      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              width: 300.0,
              margin: EdgeInsets.fromLTRB(55.0, 25.0, 55.0, 0),
              child: Column(
                children: [
                  InfoContainer('Johanes Christian', global_admin_type, '082312334620'),
                  NavigationBox(),
                ],
              ),
            ),
          ],
        ),
      )
    );
  }
}

class NavigationBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.symmetric(vertical:30, horizontal:0),
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextButton(
            onPressed: () {
              print('Dashboard Nav is clicked');
            },
            child: const Text(
              'Dashboard',
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Roboto',
                fontSize: 25,
                ),
              ),
          ),

          FadingArrow(),
        ],
      ),
      decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: const BorderRadius.all(
            Radius.circular(50.0),
          ),
        ),
    );
  }
}

class FadingArrow extends StatefulWidget {
  @override
  _FadingArrowState createState() => _FadingArrowState();
}

class _FadingArrowState extends State<FadingArrow> {
  bool _visible = true;
  
  _FadingArrowState() {
    Timer.periodic(Duration(milliseconds:1000), (Timer t) {
      setState(() {
        _visible = !_visible;
      });
    });
  }


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AnimatedOpacity(
          child: const Icon(
            Icons.arrow_right_alt_rounded,
            size: 60.0,
            color: Colors.white,
            ),
          duration: const Duration(milliseconds: 500),
          opacity: _visible ? 1.0 : 0.0,
        ),
      ],
    );
  }
}


class AdminBasedDrawer extends StatelessWidget {

  final String adminType;

  AdminBasedDrawer(this.adminType);

  @override
  Widget build(BuildContext context) {
    String dashboardTitle;
    
    if(adminType == 'tracker') {
      dashboardTitle = 'Tracking Admin Dashboard';
    } else {
      dashboardTitle = 'Generator Admin Dashboard';
    }

    return Drawer(
        child: ListView(
          children: [
            ListTile(
              title: Align(
                child: Text(
                  'BansosPeduli',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontFamily: 'Roboto',
                    fontSize: 30,
                    fontWeight: FontWeight.w900,
                  ),
                ),
                alignment: Alignment(-1.0, 0),
              ),
            ),
            
            const Divider(
              color: Colors.grey,
            ),

            ListTile(
              leading: Icon(Icons.account_circle),
              title: const Align(
                child: Text('My Profile'),
                alignment: Alignment(-1.2, 0),
              ),

              onTap: (){
                print('My profile is clicked');
              },
            ),

            ListTile(
                leading: Icon(Icons.dashboard),
                title: Align(
                  child: Text(dashboardTitle),
                  alignment: Alignment(-1.9, 0),
                ),

                onTap: (){
                  if(adminType == 'tracker')
                    print('Tracking Dashboard is clicked');
                  else
                    print('Generator Dasboard is Clicker');
                },
            ),

            ListTile(
                leading: Icon(Icons.logout),
                title: const Align(
                  child: Text('Logout'),
                  alignment: Alignment(-1.2, 0),
                ),

                onTap: (){
                  print('Logout is clicked');
                },
            ),
          ]
        ),
      );
  }
}


class InfoContainer extends StatelessWidget {

  final String name;
  final String adminType;
  final String phoneNumber;

  InfoContainer(this.name, this.adminType, this.phoneNumber);

  @override
  Widget build(BuildContext context) {
    return Material(
          elevation: 3.0,
          borderRadius: BorderRadius.circular(30.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                padding: const EdgeInsets.symmetric(vertical:10, horizontal: 68),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(30.0),
                    topRight: Radius.circular(30.0),
                  ),
                ),
                child: const Center(
                  child: Text(
                    'My Profile',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),

              Container(
                margin: const EdgeInsets.symmetric(vertical: 15),
                child: const Image(
                  image: AssetImage('assets/profile.png'),
                  height: 100,
                  width: 100,
                ),
              ),

              Container(
                margin: const EdgeInsets.symmetric(vertical: 2.5),
                padding: const EdgeInsets.symmetric(vertical: 5.6, horizontal: 10.4),
                decoration: BoxDecoration(
                  color: Colors.grey[600],
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: const Text(
                  'Admin',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                  ),
                ),
              ),

              Container(
                margin: const EdgeInsets.symmetric(vertical: 2.5),
                child: Text(
                  name,
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ),

              Container(
                margin: const EdgeInsets.symmetric(vertical: 2.5),
                child: Text(
                  adminType,
                  style: const TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.normal,
                    color: Colors.black45,
                    fontFamily: 'Roboto',
                  ),
                ),
              ),

              Container(
                margin: EdgeInsets.symmetric(vertical: 2.5),
                child: Text(
                  'Registered Phone Number:',
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ),

              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 10.0),
                child: Text(
                  phoneNumber,
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ),

              Container(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 20.0),
                child: ElevatedButton(
                  onPressed: (){},
                  child: const Text(
                      'Log Out',
                      style: TextStyle(
                        color: Colors.white,
                      )
                  ),
                  style: ElevatedButton.styleFrom(primary:Colors.red[800]),
              ),
              ),
            ],
          ),
        );
  }
}